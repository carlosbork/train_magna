import os
import zipfile
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient


directory_to_zip = '/app/runs/train/exp'
zip_filename = '/app/res.zip'


CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=zncstramvision;AccountKey=CkGiML7yqS8Z7CGzM4ffSKDIuFc3T0Lfte6lvb5sx1JcjQosUqkZSOj+VWkLoNDSqm0lPCilf2co+ASt8FE1PA==;EndpointSuffix=core.windows.net"
CONTAINER_NAME = "data-train"


# Comprimir el directorio
with zipfile.ZipFile(zip_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
    for root, dirs, files in os.walk(directory_to_zip):
        for file in files:
            zipf.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), directory_to_zip))


try:
    blob_service_client = BlobServiceClient.from_connection_string(CONNECTION_STRING)
    container_client = blob_service_client.get_container_client(CONTAINER_NAME)
    blob_name = os.path.basename(zip_filename)
    blob_client = container_client.get_blob_client(blob=blob_name)

    with open(zip_filename, "rb") as data:
        blob_client.upload_blob(data, overwrite=True)

    print(f"La imagen '{blob_name}' se ha cargado correctamente en el contenedor '{CONTAINER_NAME}'.")
except Exception as e:
    print(f"Se produjo un error al cargar la imagen en Azure Blob Storage: {e}")