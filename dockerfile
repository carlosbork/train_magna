FROM python:3.11

WORKDIR /app

COPY . .

RUN pip install -r requirements.txt

RUN apt-get update && apt-get install -y \
    libgl1-mesa-glx \
    libgl1-mesa-dri


COPY execution.sh .
RUN chmod +x /app/execution.sh

ENTRYPOINT ["/app/execution.sh"]
