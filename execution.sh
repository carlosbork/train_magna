#!/bin/bash

echo "Descargando el modelo desde S3..."
python get_model_s3.py
if [ $? -eq 0 ]; then
    echo "Descarga completada"
else
    echo "Error en la descarga del modelo desde S3. Terminando el script."
    sudo shutdown -h now
    exit 1
fi

echo "Wait... para descomprimir"
sleep 150

echo "Descomprimiendo data..."
unzip -q /app/train_data.zip -d /app/
if [ $? -eq 0 ]; then
    echo "Data descomprimida"
else
    echo "Error al descomprimir los datos. Terminando el script."
    sudo shutdown -h now
    exit 1
fi

echo "Wait... para entrenar"
sleep 60

echo "Iniciando el entrenamiento..."
python train.py --img 640 --batch 16 --epochs 10 --data coco128.yaml --weights yolov5s.pt --cache
if [ $? -eq 0 ]; then
    echo "Entrenamiento completo"
else
    echo "Error durante el entrenamiento. Terminando el script."
    sudo shutdown -h now
    exit 1
fi

echo "Comprimiendo datos..."
python comprimir.py
if [ $? -eq 0 ]; then
    echo "Proceso terminado satisfactoriamente!!"
else
    echo "Error al comprimir datos. Terminando el script."
    sudo shutdown -h now
    exit 1
fi

echo "Wait...."
sleep 300

echo "Apagando la VM..."
sudo shutdown -h now
